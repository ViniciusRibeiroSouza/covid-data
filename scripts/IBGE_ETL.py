#!/usr/bin/env python
# coding: utf-8

import os
import re
import csv
import glob
import pandas
import sidrapy
from glob import glob

COLUMNS = ['Unidade da Federação (Código)', 'Unidade da Federação',
           'Ano (Código)', 'Ano', 'Variável (Código)', 'Variável',
           'Unidade de Medida', 'Valor']


class DataHandler:
    """
    Class to handle data extraction and folder allocation
    """

    def __init__(self, raw_data_folder, index_folder, prep_data_path, metadata_folder):
        self.current_path = os.getcwd()
        self.raw_data = os.path.join(self.current_path.replace('/scripts', ''), raw_data_folder)
        self.index_path = os.path.join(self.current_path.replace('/scripts', ''), index_folder)
        self.metadata_path = os.path.join(self.current_path.replace('/scripts', ''), metadata_folder)
        self.prep_data_path = os.path.join(self.current_path.replace('/scripts', ''), prep_data_path)
        self.columns = COLUMNS
        self.dataframe_index = None
        self.current_dataframe = None

    def fix_header(self, dataframe):
        """
        Return fixed header dataframe , remove first line from Sidra IBGE response that is not
        the dataframe head
        :param dataframe: dataframe
        """
        new_header = dataframe.iloc[0]  # grab the first row for the header
        dataframe = dataframe[1:]  # take the data less the header row
        dataframe.columns = new_header  # set the header row as the df header
        self.current_dataframe = dataframe

    def extract_from_sidra(self, table_code, group, territorial_level='3',
                           classification=None, namespace='',
                           ibge_territorial_code="all", variable='all', period='all'):
        """
        :param group: data source origin
        :param namespace: namespace domain
        :param table_code: code of the table from which you want to extract the data
        :param territorial_level:  specify the territorial levels and their
        desired territorial units ( 3 = federation unit)
        :param classification: specify the table classifications and their desired categories.
        :param ibge_territorial_code: specify one of the federation unit
        :param variable: specify the desired variables
        :param period: specify the desired periods (months, years, etc.)
        """
        dataframe = sidrapy.get_table(table_code, territorial_level=territorial_level,
                                      classification=classification, ibge_territorial_code=ibge_territorial_code,
                                      variable=variable, period=period)
        DataHandler.write_csv(dataframe, os.path.join(self.raw_data, group, namespace, str(table_code))+'.csv')

    def clean_dataframe(self):
        """
        adjust the dataframe to be used
        """
        file_path_array = DataHandler.get_files_from_dir(self.raw_data, 'csv')
        for file_path in file_path_array:
            table_code = file_path.split('/')[-1].replace('.csv', '')
            self.current_dataframe = pandas.read_csv(file_path)
            self.fix_header(self.current_dataframe)
            self.current_dataframe = self.current_dataframe[self.current_dataframe['Unidade de Medida'] != '%']
            if 'Trimestre' in self.current_dataframe.columns:
                self.columns = [column for column in COLUMNS
                                if not (column in ['Ano', 'Ano (Código)'])]
                self.columns.extend(['Trimestre (Código)', 'Trimestre'])
            self.current_dataframe = self.current_dataframe.loc[:, self.current_dataframe.columns.isin(self.columns)]
            DataHandler.write_csv(self.current_dataframe, '{0}/{1}.csv'.format(self.prep_data_path, table_code))

    def read_index_files(self):
        """
        read and generate metadata from the index files
        """
        index_files = DataHandler.get_files_from_dir(self.index_path, 'xlsx')
        for index_file in index_files:
            group = index_file.split('/index/')[-1].split('_')[0].lower()
            excel_handler = pandas.ExcelFile(index_file)
            sheet_names = excel_handler.sheet_names
            for sheet in sheet_names:
                data_raw = pandas.read_excel(excel_handler, sheet_name=sheet, header=None,
                                             names=['ID', 'DESCRIPTION'])
                data_prep = data_raw.dropna().reset_index(drop=True)
                data_name_space = sheet.strip().lower().split(' ')[0]
                if data_name_space[-1] == 's':
                    data_name_space = data_name_space[:-1]
                if not os.path.exists(os.path.join(self.metadata_path, group, data_name_space)):
                    print("{0} {1} - Created".format("path", os.path.join(self.metadata_path, group, data_name_space)))
                    os.makedirs(os.path.join(self.metadata_path, group, data_name_space))
                else:
                    print("{0} {1} - OK".format("path", os.path.join(self.metadata_path, group, data_name_space)))
                DataHandler.write_csv(data_prep,
                                      os.path.join(self.metadata_path, group, data_name_space, sheet + '.csv'))

    def extract_tables(self, limit=80):
        sub_folders = glob(os.path.join(self.metadata_path, '*', '*', '*.csv'))
        table_extracted_list = [["table_code", "namespace", "status"]]
        count = 0
        for file in sub_folders:
            group = file.split('/')[-3]
            namespace = file.split('/')[-2]
            if not os.path.exists(os.path.join(self.raw_data, group, namespace)):
                os.makedirs(os.path.join(self.raw_data, group, namespace))
            index_dataframe = pandas.read_csv(file)
            index_dataframe_id = index_dataframe['ID'].to_numpy()
            for table_code in index_dataframe_id:
                table_codes_done = [path.split('/')[-1].replace('.csv', '') for path in
                                    glob(os.path.join(self.raw_data, group, namespace, '*.csv'))]
                if str(table_code) in table_codes_done:
                    print('Already Done')
                    continue
                table_metadata = [table_code, group, namespace]
                try:
                    if group == 'pnad':
                        print("Getting table_code:{0} from {1}".format(table_code, group))
                        self.extract_from_sidra(table_code=table_code, group=group,
                                                namespace=namespace, period='all')
                        table_metadata.append('OK')
                    elif group == 'censo':
                        print("Getting table_code:{0} from {1}".format(table_code, group))
                        self.extract_from_sidra(table_code=table_code, group=group,
                                                namespace=namespace, period='all')
                        table_metadata.append('OK')
                    else:
                        print('Group not defined')
                except Exception as error:
                    table_metadata.append('Error')
                    print(error)
                table_extracted_list.append(table_metadata)
                count += 1
            if count > limit:
                break
        with open(os.path.join(self.raw_data, 'log.csv'), 'w', newline='') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerows(table_extracted_list)

    @staticmethod
    def write_csv(dataframe, filepath, compression=None):
        dataframe.to_csv(filepath, index=False, encoding='utf-8', compression=compression)

    @staticmethod
    def get_files_from_dir(folder, type):
        return glob.glob('{0}/*.{1}'.format(folder, type))


if __name__ == '__main__':
    df_handler = DataHandler('raw_data', 'index', 'prep_data', 'metadata')
    df_handler.extract_tables()
